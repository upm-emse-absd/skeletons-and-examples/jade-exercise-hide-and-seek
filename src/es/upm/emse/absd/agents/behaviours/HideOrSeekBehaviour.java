package es.upm.emse.absd.agents.behaviours;

import es.upm.emse.absd.Utils;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.Random;

public class HideOrSeekBehaviour extends ParallelBehaviour {

    private final Random coin;

    private boolean registered = false;

    public HideOrSeekBehaviour(Agent a, long period) {

        coin = new Random();

        this.addSubBehaviour(new TickerBehaviour(a, period) {
            @Override
            protected void onTick() {
                play();
            }
        });
        this.addSubBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                perceive();
            }
        });


    }
    private void play() {
        if(coin.nextBoolean() && !registered) {
            //System.out.println(this.getAgent().getLocalName() + ": Moving!!!");
            Utils.register(this.getAgent(), "moving");
            registered = true;
        }
        else if (registered) {
            //System.out.println(this.getAgent().getLocalName() + ": Shhh...");
            Utils.deregister(this.getAgent());
            registered = false;
        }
    }

    private void perceive() {
        ACLMessage msg = this.getAgent().receive();
        if (msg != null) {
            System.out.println(this.getAgent().getLocalName() + ": I'm dead");
            Utils.deregister(this.getAgent());
            this.getAgent().doSuspend();
        }
    }
}
