package es.upm.emse.absd.agents.behaviours;

import es.upm.emse.absd.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;

public class KillBehaviour extends SequentialBehaviour {

    public KillBehaviour(Agent a, long period, String service) {

        addSubBehaviour(new WakerBehaviour(a, period) {
            @Override
            protected void onWake() {
                super.onWake();
            }
        });
        addSubBehaviour(new TickerBehaviour(a, period) {
            @Override
            protected void onTick() {
                //System.out.println(this.myAgent.getLocalName() + ": Stop!!!");
                AID[] victims = Utils.searchDF(this.getAgent(), service);
                if (victims!=null) kill(victims);
            }
        });

    }

    private void kill(AID[] agents) {
        for (AID victim : agents) {
            System.out.println(this.myAgent.getLocalName() + ": participant " + victim.getLocalName() + " eliminated." );
            ACLMessage killMsg = Utils.newMsg(this.myAgent, ACLMessage.INFORM, null, victim);
            this.getAgent().send(killMsg);
        }
    }


}
