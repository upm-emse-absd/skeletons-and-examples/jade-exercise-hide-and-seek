package es.upm.emse.absd.agents;

import es.upm.emse.absd.agents.behaviours.KillBehaviour;
import jade.core.Agent;

public class SeekerAgent extends Agent {

    protected void setup()
    {
        System.out.println("Hello!!! My name is " + this.getLocalName() + ". Nice to meet you :D");
        addBehaviour(new KillBehaviour(this, 3000, "moving"));
    }

}
