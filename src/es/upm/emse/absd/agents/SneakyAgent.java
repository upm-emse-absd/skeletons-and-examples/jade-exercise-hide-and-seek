package es.upm.emse.absd.agents;

import es.upm.emse.absd.agents.behaviours.HideOrSeekBehaviour;
import jade.core.Agent;

public class SneakyAgent extends Agent {

    protected void setup()
    {
        System.out.println("Hello!!! My name is " + this.getLocalName() + ". Nice to meet you :D");
        addBehaviour(new HideOrSeekBehaviour(this, 2000));
    }

}
