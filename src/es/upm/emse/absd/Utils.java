package es.upm.emse.absd;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

public final class Utils {

    private static int cidCnt = 0;
    private static String cidBase;

    /**
     * Generate Conversation ID attached to an agent
     * @param agent the agent
     * @return the ID generated
     */
    public static String genCID(Agent agent)
    {
        if (cidBase==null) {
            cidBase = agent.getLocalName() + agent.hashCode() +
                System.currentTimeMillis()%10000 + "_";
        }
        return  cidBase + (cidCnt++);
    }

    /**
     * New ACLMessage given the sender, the FIPA performative type, serialized content, and receiver.
     * @param origin agent sender
     * @param perf FIPA performative
     * @param content serialized text content
     * @param dest Agent ID destination
     * @return the ACLMessage ready to be sent.
     */
    public static ACLMessage newMsg(Agent origin, int perf, String content, AID dest)
    {
        ACLMessage msg = newMsg(origin, perf);
        if (dest != null) msg.addReceiver(dest);
        msg.setContent( content );
        return msg;
    }

    /**
     * New ACLMessage given the sender and the FIPA performative without content and sender,.
     * @param origin agent sender
     * @param perf FIPA performative
     * @return the ACLMessage ready to be sent.
     */
    public static ACLMessage newMsg(Agent origin, int perf)
    {
        ACLMessage msg = new ACLMessage(perf);
        msg.setConversationId(genCID(origin));
        return msg;
    }

    /**
     * Function for register an agent to the yellow pages (Directory Facilitator service).
     * @param agent the agent
     * @param type the service to be registered
     * @return True if the register is successful.
     */
    public static boolean register(Agent agent, String type)
    {
        ServiceDescription sd  = new ServiceDescription();
        sd.setType(type);
        sd.setName(agent.getLocalName());
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(agent.getAID());
        dfd.addServices(sd);

        try {
            DFService.register(agent, dfd );
            return true;
        }
        catch (FIPAException fe) {
            System.err.println(fe.getMessage());
            return false;
        }
    }

    /**
     * Deregister an agent from the yellow pages (DF Service).
     * @param agent the agent
     * @return True if the register is successful.
     */
    public static void deregister(Agent agent) {
        try { DFService.deregister(agent); }
        catch (Exception e) { System.err.println(e.getMessage()); }
    }

    /**
     * Get all the agent IDs from the yellow pages registered to a service.
     * @param agent the searcher agent
     * @param service the service
     * @return An array of AIDs if the service exists, null otherwise.
     */
    public static AID[] searchDF(Agent agent, String service)
    {
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType( service );
        dfd.addServices(sd);

        try
        {
            DFAgentDescription[] result = DFService.search(agent, dfd);
            AID[] agents = new AID[result.length];
            for (int i=0; i<result.length; i++)
                agents[i] = result[i].getName();
            return agents;

        }
        catch (FIPAException e) { return null; }

    }


}
